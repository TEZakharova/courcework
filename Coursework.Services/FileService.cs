﻿using Coursework.Services.Abstraction;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Collections.Generic;
using System.Linq;

namespace Coursework.Services
{
    public class FileService : IFileService
    {
        public string OpenFile(string path)
        {
            using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(path, false))
            {
                Body body = wordprocessingDocument.MainDocumentPart.Document.Body;
                Paragraph firstParagraph = body.Elements<Paragraph>().FirstOrDefault();
                DocumentFormat.OpenXml.OpenXmlElement firstChild = firstParagraph.FirstChild;
                IEnumerable<Run> elementsAfter = firstChild.ElementsAfter().Where(c => c is Run).Cast<Run>();
                wordprocessingDocument.Close();

                string result = string.Empty;

                foreach (var text in body.Descendants<Text>())
                {
                    result += $"{text.Text}\n";
                }

                return result;
            }
        }
    }
}
