﻿namespace Coursework.Services.Abstraction
{
    public interface IFileService
    {
        string OpenFile(string path);
    }
}
