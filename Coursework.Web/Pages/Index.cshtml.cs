﻿using Coursework.Services;
using Coursework.Services.Abstraction;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Coursework.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        private ICryptoService CryptoService { get; } 

        private IFileService FileService { get; }

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
            CryptoService = new CryptoService();
            FileService = new FileService();
        }
        
        public string FirstTextarea { get; set; }
        
        public string SecondTextarea { get; set; }
       

        public void OnPostStart(string resultDec, string key, int eventHandling)
        {
            try
            {
                if(eventHandling == 0)
                {
                    SecondTextarea = CryptoService.DecodeText(resultDec, key);
                }
                else
                {
                    SecondTextarea = CryptoService.EncodeText(resultDec, key);
                }
            }
            catch
            {
                SecondTextarea = "Ошибка!\nВведите текст!";
            }
        }
        public void OnPostOpenFile(string fileOpen)
        {
            try
            {
                FirstTextarea = FileService.OpenFile(fileOpen);
            }
            catch
            {
                FirstTextarea = "Файл ненайден!";
            }
        }

        public void OnPostRadio(string resultDec, string resultEnc)
        {
            if (string.IsNullOrEmpty(resultDec))
            {
                SecondTextarea = resultDec;
                FirstTextarea = resultEnc;
            }
            else
            {
                SecondTextarea = resultDec;
                FirstTextarea = resultEnc;
            }
        }
    }
}