using Coursework.Services;
using Coursework.Services.Exceptions;
using Coursework.Services.Abstraction;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Courcework.Services.Tests
{
    [TestClass]
    public class CryptoServiceTests
    {
        private ICryptoService CryptoService { get; set; }

        private readonly string HelloWorldString = "������ ���";

        [TestInitialize]
        public void Run()
        {
            CryptoService = new CryptoService();
        }

        [DataTestMethod]
        [DataRow("������", "������")]
        [DataRow("�������", "������")]
        [DataRow("�������", "�������")]
        [DataRow("������", "������")]
        public void EncodeText_NormalUsage_Success(string text, string expected)
        {
            Assert.AreEqual(expected, CryptoService.EncodeText(text, HelloWorldString));
        }

        [DataTestMethod]
        [DataRow("")]
        [DataRow(null)]
        [DataRow("   ")]
        public void EncodeText_EmptySourceText_Exception(string sourceText)
        {
            Assert.ThrowsException<SourceTextIsNullOrEmptyException>(() => CryptoService.EncodeText(sourceText, HelloWorldString));
        }

        [DataTestMethod]
        [DataRow("")]
        [DataRow(null)]
        [DataRow("   ")]
        public void EncodeText_EmptyKey_Exception(string key)
        {
            Assert.ThrowsException<KeyIsNullOrEmptyException>(() => CryptoService.EncodeText(HelloWorldString, key));
        }

        [DataTestMethod]
        [DataRow("������", "������")]
        [DataRow("������", "�������")]
        [DataRow("�������", "�������")]
        [DataRow("������", "������")]
        public void DecodeText_NormalUsage_Success(string text, string expected)
        {
            Assert.AreEqual(expected, CryptoService.DecodeText(text, HelloWorldString));
        }

        [DataTestMethod]
        [DataRow("")]
        [DataRow(null)]
        [DataRow("   ")]
        public void DecodeText_EmptySourceText_Exception(string sourceText)
        {
            Assert.ThrowsException<SourceTextIsNullOrEmptyException>(() => CryptoService.DecodeText(sourceText, HelloWorldString));
        }

        [DataTestMethod]
        [DataRow("")]
        [DataRow(null)]
        [DataRow("   ")]
        public void DecodeText_EmptyKey_Exception(string key)
        {
            Assert.ThrowsException<KeyIsNullOrEmptyException>(() => CryptoService.DecodeText(HelloWorldString, key));
        }
    }
}
