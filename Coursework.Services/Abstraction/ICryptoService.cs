﻿namespace Coursework.Services.Abstraction
{
    public interface ICryptoService
    {
        string DecodeText(string sourseText, string key);

        string EncodeText(string sourseText, string key);
    }
}
