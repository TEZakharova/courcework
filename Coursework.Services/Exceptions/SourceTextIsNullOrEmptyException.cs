﻿using System;

namespace Coursework.Services.Exceptions
{
    public class SourceTextIsNullOrEmptyException : Exception
    {
        public SourceTextIsNullOrEmptyException() : base("Source text is null or empty")
        {
        }
    }
}
