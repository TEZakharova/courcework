﻿using System;

namespace Coursework.Services.Exceptions
{
    public class KeyIsNullOrEmptyException : Exception
    {
        public KeyIsNullOrEmptyException() : base("Key is null or empty")
        {
        }
    }
}
