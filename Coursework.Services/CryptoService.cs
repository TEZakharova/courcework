﻿using Coursework.Services.Abstraction;
using Coursework.Services.Exceptions;
using System;

namespace Coursework.Services
{
    public class CryptoService : ICryptoService
    {
        public string DecodeText(string sourseText, string key)
        {
            if (string.IsNullOrWhiteSpace(sourseText))
            {
                throw new SourceTextIsNullOrEmptyException();
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new KeyIsNullOrEmptyException();
            }

            char[] abc = { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' };
            char[] message = sourseText.ToCharArray();
            char[] keyChar = key.ToCharArray();
            int j, number, index, d;
            int t = 0;
            for (int i = 0; i < message.Length; i++)
            {
                for (j = 0; j < abc.Length; j++)
                {
                    if (message[i] == abc[j])
                        break;
                }
                if (j != 33)
                {
                    number = j;
                    if (t > keyChar.Length - 1)
                    {
                        t = 0;
                    }
                    for (index = 0; index < abc.Length; index++)
                    {
                        if (keyChar[t] == abc[index])
                        {
                            break;
                        }
                    }
                    t++;
                    if (index != 33)
                    {
                        d = number + index;
                    }
                    else
                    {
                        d = number;
                    }
                    if (d > 32)
                    {
                        d = d - 33;
                    }
                    message[i] = abc[d];
                }
            }
            return new string(message);
        }

        public string EncodeText(string sourseText, string key)
        {
            if (string.IsNullOrWhiteSpace(sourseText))
            {
                throw new SourceTextIsNullOrEmptyException();
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new KeyIsNullOrEmptyException();
            }

            int number;
            int d;
            int j, f;
            int t = 0;
            char[] message = sourseText.ToCharArray();
            char[] keyChar = key.ToCharArray();
            char[] abs = { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' }; // Массив алфавита

            for (int i = 0; i < message.Length; i++)
            {
                for (j = 0; j < abs.Length; j++)
                {
                    if (message[i] == abs[j])
                    {
                        break;
                    }
                }

                if (j != 33)
                {
                    number = j;
                    if (t > keyChar.Length - 1)
                    {
                        t = 0;
                    }

                    for (f = 0; f < abs.Length; f++)
                    {
                        if (keyChar[t] == abs[f])
                        {
                            break;
                        }
                    }

                    t++;
                    if (f != 33)
                    {
                        d = number - f + abs.Length;
                    }
                    else
                    {
                        d = number;
                    }
                    if (d > 32)
                    {
                        d = d - 33;
                    }
                    message[i] = abs[d];
                }

            }

            return new string(message);
        }
    }
}
